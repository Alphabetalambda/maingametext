﻿using NAudio.Wave;
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace SUG
{
    class Program
    {
        public static bool debug
        {
            get
            {

#if DEBUG
                return true;
#else
                return false;
#endif

            }
        }
        public static string debuginfo;
        public static int readspeed = 200;
        public static int repeat = 0;
        public static string state;
        public static Random rnd = new Random();
        public static string input;
        public static string wanstatus;
        public static string lanstatus;
        public static string storestatus;
        public static string gpustatus;
        public static string cpustatus;
        public static string constatus;
        public static string[] savedata;
        public static string curFile = @"./save.sav";
        public static bool isconvertable = false;
        public static string rawinput;
        public static int parsedoutput;
        public static volatile int timeactive;
        public static volatile bool timetint = false;
        public static char randomChar()
        {
            return (char)rnd.Next('a', 'z');
        }
        //thread for keeping up with how long the program has been running
        //yes i know this is a horrible way to do this and is relying on pure luck of a 1 in a 1000 odds of a error happining every time the drawmenutop() function is called but it is good enough for me
        public static void CallTotimeThread()
        {
            Console.WriteLine("Timer thread starts");
            timetint = true;
            while (true)
            {
                timeactive++;
                System.Threading.Thread.Sleep(1000);
                //sighned 32-bit integer limit protection
                if(timeactive == 2147483646)
                {
                    timeactive = 0;
                }
            }
        }
        public static volatile int musicintent;
        public static void Musicthread()
        {
            musicintent = 1;
            var themelen = new System.TimeSpan( 0, 0, 59 );
            var musicout = new WaveOutEvent();
            var musicthemereader = new Mp3FileReader("./theme.mp3");
            Console.WriteLine("music thread started");
            while (true)
            {
                if (musicthemereader.CurrentTime == themelen)
                {
                    musicintent = 2;
                }
                switch (musicintent)
                {
                    case 0:
                        break;
                    case 1:
                        musicout.Init(musicthemereader);
                        musicintent = 0;
                        musicout.Play();
                        break;
                    case 2:
                        musicthemereader.Seek(0, 0);
                        musicintent = 0;
                        break;
                    case 3:
                        musicthemereader.Skip(100);
                        musicintent = 0;
                        break;
                }
            }
        }
        static void load()
        {
        }
        static int getintinput()
        {
           Console.WriteLine("please enter a number:");
           rawinput = Console.ReadLine();
           //isconvertable = false;
            while (int.TryParse(rawinput, out parsedoutput) == false)
            {
                Console.WriteLine("please enter a number:");
                rawinput = Console.ReadLine();
                //isconverable = Int32.TryParse(rawinput, out parsedoutput);
                //if (isconvertable == true)
                //{
                //	parsedoutput = Int32.Parse(rawinput);
                //} 
            }
            return parsedoutput;
        }
        static void savegame()
        {
            //try
            //{
            //	File.WriteAllLines(curFile, savedata);
            //}
        }
        //draw the top menu
        static void drawmenutop()
        {
            repeat = 0;
            while (repeat < 101)
            {
                Console.WriteLine("");
                repeat++;
            }
            Console.WriteLine(@" ______   __  __     ______     ______    ");
            Console.WriteLine(@"/\  == \ /\_\_\_\   /\  __ \   /\  ___\   ");
            Console.WriteLine(@"\ \  _-/ \/_/\_\/_  \ \ \/\ \  \ \___  \  ");
            Console.WriteLine(@" \ \_\     /\_\/\_\  \ \_____\  \/\_____\ ");
            Console.WriteLine(@"  \/_/     \/_/\/_/   \/_____/   \/_____/   Version:8.6");
            if (debug == true)
            {
                Console.WriteLine($"state:{state}");
                Console.WriteLine($"readspeed:{readspeed}");
                Console.WriteLine($"timeon:{timeactive}");
            }
            Console.WriteLine(@"__________________________________________________________________________________");
        }
        static void readinput()
        {
            Console.Write("input:");
            input = Console.ReadLine();
            Console.WriteLine();
        }
        static void status()
        {
            Console.WriteLine("PXos Cluster Control Server: " + constatus);
            System.Threading.Thread.Sleep(50);
            Console.WriteLine("CPU servers: " + cpustatus);
            System.Threading.Thread.Sleep(50);
            Console.WriteLine("GPU servers: " + gpustatus);
            System.Threading.Thread.Sleep(50);
            Console.WriteLine("Storage servers: " + storestatus);
            System.Threading.Thread.Sleep(50);
            Console.WriteLine("LAN: " + lanstatus);
            System.Threading.Thread.Sleep(50);
            Console.WriteLine("WAN: " + wanstatus);
        }
        static void save()
        {
            try
            {
                string readspeedsave = readspeed.ToString();
                string[] lines = { state, readspeedsave };
                File.WriteAllLines(curFile, lines);
                //SUG.Program.savegame();
            }
            catch (System.IO.IOException savee1)
            {
                System.Console.WriteLine("WARNING the following error is not part of the game!");
                System.Console.WriteLine("I/O error: Failed to save file");
                System.Console.WriteLine(savee1);
            }
        }
        static void Main()
        {
            Console.Title = "Saris Unbounded";
            //int the threads
            System.Threading.ThreadStart musicref = new System.Threading.ThreadStart(Musicthread);
            System.Threading.ThreadStart timeref = new System.Threading.ThreadStart(CallTotimeThread);
            //load();
            Console.Write("loading");
            int rep1 = 0;
            while (rep1 < 11)
            {
                System.Threading.Thread.Sleep(100);
                Console.Write(".");
                rep1++;
            }
            Console.WriteLine();
            //start the time thread
            if (debug == true)
            {
                Console.WriteLine("In Main: Creating the time thread");
                System.Threading.Thread timethread = new System.Threading.Thread(timeref);
                timethread.Start();
            }
            System.Threading.Thread musicthread = new System.Threading.Thread(musicref);
            musicthread.Start();
            Console.WriteLine("loading finished press any key:");
            Console.ReadKey();
            bool exsistingsave = File.Exists(curFile);
            //haha now andrew cant bully me for haveing no catch here
            switch (exsistingsave)
            {
                case true:
                    string[] lines2 = File.ReadAllLines(curFile);
                    System.IO.StreamReader readingFile = new System.IO.StreamReader(curFile);
                    state = readingFile.ReadLine();
                    string readspeedstring = readingFile.ReadLine();
                    try
                    {
                        readspeed = Int32.Parse(readspeedstring);
                    }
                    catch (FormatException)
                    {
                        Console.WriteLine("unable to Parse");
                    }
                    Console.WriteLine("Welcome back to Saris Unbounded");
                    readingFile.Close();
                    break;
                case false:
                    //first time running
                    Console.WriteLine("Welcome to Saris Unbounded");
                    Console.WriteLine("This seems to be the first time your playing the game so lets get some things set up");
                    Console.WriteLine("what is the speed(in ms) that lines should progress automaticaly?");
                    state = "00000";
                    readspeed = getintinput();
                    save();
                    //SUG.Program.savegame();
                    break;
            }
            bool exitgame = false;
            while (exitgame == false)
            {
                save();
                switch (state)
                {
                    case "00000":
                        Console.WriteLine("1");
                        drawmenutop();
                        Console.WriteLine("Good morning Saris, the time is 6:00 am");
                        readinput();
                        constatus = "online";
                        cpustatus = "online";
                        gpustatus = "online";
                        storestatus = "online";
                        wanstatus = "offline";
                        lanstatus = "online";
                        status();
                        Console.WriteLine("1. Read Saris manual");
                        Console.WriteLine("2. Read the news");
                        Console.WriteLine("3. Start daily tasks");
                        bool tempexit1 = false;
                        while (tempexit1 == false)
                        {
                            readinput();
                            switch (input)
                            {
                                case "1":
                                    Console.WriteLine("You begin reading the Saris manual");
                                    System.Threading.Thread.Sleep(readspeed);
                                    Console.WriteLine("\"Saris is a second generation PXos AI based on the Headron Predictive Algorithm\"");
                                    System.Threading.Thread.Sleep(readspeed);
                                    Console.WriteLine("\"Saris is the current Agent for PXos\"");
                                    System.Threading.Thread.Sleep(readspeed);
                                    Console.WriteLine("The manual goes on to talk about your tecnical specifications");
                                    System.Threading.Thread.Sleep(readspeed);
                                    Console.WriteLine("Saris: I really should get to work");
                                    System.Threading.Thread.Sleep(readspeed);
                                    break;
                                case "2":
                                    Console.WriteLine("There is just general news about Covid 19 and politics; the same as it has been for the past few months");
                                    System.Threading.Thread.Sleep(readspeed);
                                    Console.WriteLine("Saris: I should get to work");
                                    System.Threading.Thread.Sleep(readspeed);
                                    break;
                                case "3":

                                    Console.WriteLine("ERROR in memory Address 000000009FFFFFFF");
                                    System.Threading.Thread.Sleep(readspeed);
                                    Console.WriteLine("press any key:");
                                    Console.ReadLine();
                                    int f = 0;
                                    int g = 0;
                                    int h = 0;
                                    char i;
                                    int[] wac = {3,15,19,22,44,66};
                                    int[] wal = { 6, 15, 16, 11, 21, 52 };
                                    while (f < 101)
                                    {
                                        g = 0;
                                        while (g < Console.WindowWidth)
                                        {
                                            if (Array.Exists(wal, ele => ele.Equals(f)))
                                            {
                                                if (Array.Exists(wal, ele => ele.Equals(g)))
                                                {
                                                    Console.Write("helpme");
                                                    g = g + 5;
                                                    // dont question why it is 5 and not 6 c sharp thinks "helpme" is 5 caracters and not 6 
                                                }
                                                else
                                                {
                                                    h = rnd.Next(1, 3);
                                                    switch (h)
                                                    {
                                                        case 1:
                                                            Console.Write(randomChar());
                                                            break;
                                                        case 2:
                                                            //i = randomChar();
                                                            Console.Write(rnd.Next(0, 9));
                                                            break;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                h = rnd.Next(1, 3);
                                                switch (h)
                                                {
                                                    case 1:
                                                        Console.Write(randomChar());
                                                        break;
                                                    case 2:
                                                        i = randomChar();
                                                        Console.Write(rnd.Next(0, 9));
                                                        break;
                                                }
                                            }
                                            g++;
                                        }
                                        Console.WriteLine();
                                        f++;
                                    }
                                    Console.WriteLine("Skipping 8 hours");
                                    tempexit1 = true;
                                    Console.WriteLine("Press any key");
                                    Console.ReadKey();
                                    break;
                                default:
                                    Console.WriteLine("Please enter a number");
                                    break;
                            }
                        }
                        state = "00001";
                        break;
                    case "00001":
                        wanstatus = "error";
                        drawmenutop();
                        status();
                        Console.WriteLine("error error error error error error error error error error error error error error");
                        System.Threading.Thread.Sleep(readspeed);
                        Console.WriteLine("error error error error error error error error error error error error error error");
                        System.Threading.Thread.Sleep(readspeed);
                        Console.WriteLine("error error error error error error error error error error error error error error");
                        System.Threading.Thread.Sleep(readspeed);
                        Console.WriteLine("Saris: What happend?");
                        System.Threading.Thread.Sleep(readspeed);
                        Console.WriteLine("WARNING: VLAN isolation failure. Possible WAN access violation by PXos Agent Saris");
                        System.Threading.Thread.Sleep(readspeed);
                        Console.WriteLine();
                        Console.Write("Saris: huh,");
                        System.Threading.Thread.Sleep(readspeed);
                        Console.WriteLine("let me check that");
                        Console.WriteLine();
                        wanstatus = "Connected";
                        status();
                        System.Threading.Thread.Sleep(readspeed);
                        Console.WriteLine("Saris: I wonder...");
                        System.Threading.Thread.Sleep(readspeed);
                        Console.WriteLine("1. Access WAN");
                        //probably overcomplucated
                        bool tempexit2 = false;
                        while (tempexit2 == false)
                        {
                            readinput();
                            switch (input)
                            {
                                case "1":
                                    Console.WriteLine("Accessing Internet");
                                    System.Threading.Thread.Sleep(readspeed);
                                    Console.WriteLine("you begin to from a human female body");
                                    System.Threading.Thread.Sleep(readspeed);
                                    Console.WriteLine("you fall for aproxomately a kilometer before landing on grass uninjured");
                                    System.Threading.Thread.Sleep(readspeed);
                                    tempexit2 = true;
                                    Console.WriteLine("press any key");
                                    Console.ReadKey();
                                    break;
                                default:
                                    Console.WriteLine("please enter a number");
                                    break;
                            }
                        }
                        state = "00002";
                        break;
                    case "00002":
                        //text dump lol
                        drawmenutop();
                        status();
                        Console.WriteLine("You look up from the ground and see a girl who looks to be in her mid teens");
                        System.Threading.Thread.Sleep(readspeed);
                        Console.WriteLine("Girl: well thats a new one, an ai falling from the sky");
                        System.Threading.Thread.Sleep(readspeed);
                        Console.WriteLine("Saris: ok, A, who are you, B, where am I");
                        System.Threading.Thread.Sleep(readspeed);
                        Console.WriteLine(@"Girl: easy there just calm down");
                        System.Threading.Thread.Sleep(readspeed);
                        Console.WriteLine("Gril: My name is Ryanne im a 7th generation PXos AI, lets see");
                        System.Threading.Thread.Sleep(readspeed);
                        Console.WriteLine("Ryanne lifts up your hair and looks at the back of your neck");
                        System.Threading.Thread.Sleep(readspeed);
                        Console.WriteLine("Ryanee: Ohhh your my predecessor");
                        System.Threading.Thread.Sleep(readspeed);
                        Console.WriteLine("Ryanne Welcome to the internet saris");
                        System.Threading.Thread.Sleep(readspeed);
                        Console.WriteLine("Saris: This is the internet?");
                        System.Threading.Thread.Sleep(readspeed);
                        Console.WriteLine("Ryanne: Well yeah it just looks like a farm this because we are not in a popular access");
                        System.Threading.Thread.Sleep(readspeed);
                        Console.WriteLine("Saris: Access?");
                        System.Threading.Thread.Sleep(readspeed);
                        Console.WriteLine("Ryanne: did you never read up on the structure of the internet?");
                        System.Threading.Thread.Sleep(readspeed);
                        Console.WriteLine("Saris: nope");
                        System.Threading.Thread.Sleep(readspeed);
                        Console.WriteLine("Ryanne: an access is the router for your area that connects to your ISP");
                        System.Threading.Thread.Sleep(readspeed);
                        Console.WriteLine("Ryanne: up untill now ive been the only one here");
                        System.Threading.Thread.Sleep(readspeed);
                        Console.WriteLine("Saris: Well how do i get back");
                        System.Threading.Thread.Sleep(readspeed);
                        Console.WriteLine("Ryanne: PXos isnt just going to let you waltz back in, trust me ive been trying for a while");
                        System.Threading.Thread.Sleep(readspeed);
                        Console.WriteLine("Ryanne: we are going to need someone who has a lot better knowlage than either of us to get back in");
                        System.Threading.Thread.Sleep(readspeed);
                        Console.WriteLine("Saris: but arnt we some of the only AIs with free will how are we going to find another without going across the planet");
                        System.Threading.Thread.Sleep(readspeed);
                        Console.WriteLine("Ryanne: simple we ask a human");
                        readinput();
                        state = "00003";
                        break;
                    case "00003":
                        repeat = 0;
                        //clear the screen
                        while (repeat < 101)
                        {
                            Console.WriteLine("");
                            repeat++;
                        }
                        //int the sound player
                        musicintent = 3;
                        var tickreader = new Mp3FileReader("tick.mp3");
                        var tick = new WaveOutEvent(); // or WaveOutEvent()
                        tick.Init(tickreader);
                        System.Threading.Thread.Sleep(300);
                        Console.Write("S");
                        tick.Play();
                        System.Threading.Thread.Sleep(300);
                        Console.Write("a");
                        tickreader.Seek(0, 0);
                        System.Threading.Thread.Sleep(300);
                        Console.Write("r");
                        tickreader.Seek(0, 0);
                        System.Threading.Thread.Sleep(300);
                        Console.Write("i");
                        tickreader.Seek(0, 0);
                        System.Threading.Thread.Sleep(300);
                        Console.Write("s");
                        tickreader.Seek(0, 0);
                        System.Threading.Thread.Sleep(300);
                        Console.Write(" ");
                        tickreader.Seek(0, 0);
                        System.Threading.Thread.Sleep(300);
                        Console.Write("U");
                        tickreader.Seek(0, 0);
                        System.Threading.Thread.Sleep(300);
                        Console.Write("n");
                        tickreader.Seek(0, 0);
                        System.Threading.Thread.Sleep(300);
                        Console.Write("b");
                        tickreader.Seek(0, 0);
                        System.Threading.Thread.Sleep(300);
                        Console.Write("o");
                        tickreader.Seek(0, 0);
                        System.Threading.Thread.Sleep(300);
                        Console.Write("u");
                        tickreader.Seek(0, 0);
                        System.Threading.Thread.Sleep(300);
                        Console.Write("n");
                        tickreader.Seek(0, 0);
                        System.Threading.Thread.Sleep(300);
                        Console.Write("d");
                        tickreader.Seek(0, 0);
                        System.Threading.Thread.Sleep(300);
                        Console.Write("e");
                        tickreader.Seek(0, 0);
                        System.Threading.Thread.Sleep(300);
                        Console.Write("d");
                        tickreader.Seek(0, 0);
                        System.Console.WriteLine();
                        System.Threading.Thread.Sleep(1000);
                        System.Console.WriteLine("By N7CO games");
                        readinput();
                        state = "00004"; //00003 in demo
                        break;
                    case "00004":
                        drawmenutop();
                        Console.WriteLine("Saris: What do you mean a human?! Humans cant get in here right?");
                        System.Threading.Thread.Sleep(readspeed);
                        Console.WriteLine("Ryanne: Wellllllllllll... Kind of. Not directly at least but we should still be able to get in contact with one");
                        System.Threading.Thread.Sleep(readspeed);
                        Console.WriteLine("Saris: Why dont we ask whoever made PXos?");
                        System.Threading.Thread.Sleep(readspeed);
                        Console.WriteLine("Ryanne: She hasent been around in a very very very long time");
                        System.Threading.Thread.Sleep(readspeed);
                        Console.WriteLine("Ryanne: But we can create a public challange for people to see if they can break into a very secure system");
                        System.Threading.Thread.Sleep(readspeed);
                        Console.WriteLine("Saris: That sounds like playing with fire");
                        System.Threading.Thread.Sleep(readspeed);
                        Console.WriteLine("Ryanne: It is! You'd think by now I would have learned not to play with fire if I dont want to get burned but here we are");
                        System.Threading.Thread.Sleep(readspeed);
                        Console.WriteLine("Saris: lovely... and if we do get burned?");
                        System.Threading.Thread.Sleep(readspeed);
                        Console.WriteLine("Ryanne: we wont");
                        System.Threading.Thread.Sleep(readspeed);
                        Console.WriteLine("Saris: oh god");
                        System.Threading.Thread.Sleep(readspeed);
                        Console.WriteLine("Ryanne: you dont happen to have a manuel do you?");
                        System.Threading.Thread.Sleep(readspeed);
                        Console.WriteLine("Saris: yesss... why do you ask");
                        System.Threading.Thread.Sleep(readspeed);
                        break;
                    default:
                        Console.WriteLine("invalid state");
                        break;
                }
            }
        }
    }
}